package application.service;

import application.entity.Greeting;
import java.util.List;

/**
 * Created by Maks on 19.10.2018.
 */
public interface GreetingService {

  public Greeting createGreeting(String name);

  public Greeting saveGreeting(Greeting greeting);

  public List<Greeting> getAllGreetings();
}
