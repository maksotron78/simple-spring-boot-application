package application.service.serviceImpl;

import application.entity.Greeting;
import application.service.GreetingService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService {

  private long counter;

  private List<Greeting> dataBase = new ArrayList<>();

  @Override
  public Greeting createGreeting(String name) {
    Greeting result = new Greeting(counter++, name);
    return result;
  }

  @Override
  public Greeting saveGreeting(Greeting greeting) {
    dataBase.add(greeting);
    return greeting;
  }

  @Override
  public List<Greeting> getAllGreetings(){
    return dataBase;
  }

}
