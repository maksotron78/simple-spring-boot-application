package application.controller;

import application.entity.Greeting;
import application.service.GreetingService;
import application.service.serviceImpl.GreetingServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private GreetingService greetingService;

    @Autowired
    public GreetingController(GreetingServiceImpl greetingService) {
        this.greetingService = greetingService;
    }

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", required = false, defaultValue = "World") String name) {
        Greeting response = greetingService.createGreeting(name);
        return response;
    }

    @RequestMapping(value = "/greeting",
        method = RequestMethod.PUT,
        produces = "application/json; charset=UTF-8",
        consumes = "application/json; charset=UTF-8"
    )
    public String saveGreeting(@RequestBody Greeting greeting) {
        return greetingService.saveGreeting(greeting).toString();
    }

    @GetMapping("/greeting/all")
    public List<Greeting> getAllGreetings(){
        return greetingService.getAllGreetings();
    }

}